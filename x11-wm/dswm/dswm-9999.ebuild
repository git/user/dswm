# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/x11-wm/dswm/dswm-0.0.3.ebuild,v 1.2 2011/04/25 20:36:20 betelgeuse Exp $

EAPI="5"

inherit common-lisp autotools eutils git-2

DESCRIPTION="DSWM is a tiling, keyboard driven X11 Window Manager written entirely in Common Lisp"
HOMEPAGE="http://dss-project.org"
EGIT_REPO_URI="git://github.com/dss-project/dswm.git"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE="+sbcl +static shared clisp cmucl ecl emacs doc"

CLPACKAGE="dswm"

CDEPEND="dev-lisp/common-lisp-controller
	virtual/commonlisp
	dev-lisp/cl-ppcre
	sbcl? ( >=dev-lisp/sbcl-1.0.58
			>=dev-lisp/clx-0.7.4 )
	>=dev-lisp/asdf-2.0
	clisp? ( dev-lisp/clisp )
	cmucl? ( dev-lisp/cmucl )
	ecl? ( dev-lisp/ecls )
	!dev-lisp/asdf-binary-locations"
DEPEND="${CDEPEND}
	doc? ( sys-apps/texinfo )"
RDEPEND="x11-base/xorg-server
	emacs? ( app-emacs/slime )
	shared? ( ${CDEPEND} )"
REQUIRED_USE="^^ ( sbcl clisp cmucl ecl )
	|| ( shared static )"

S="${WORKDIR}/${P}-git"
src_prepare() {
	cd "${S}"/src
	eautoconf
	cd "${S}"
}
src_configure() {
	local with_lisp
	cd "${S}"/src
	for flag in "sbcl" "clisp" "cmucl" "ecl"; do
		use $flag && with_lisp="${flag}" && break
	done
	if [ "${with_lisp}" != "sbcl" ]; then
		ewarn "${with_lisp} support in ebuild is untested."
		ewarn "Write me if you got it to work."
	fi
	econf --with-lisp="${with_lisp}" \
		--with-dswm-source-dir="${D}/usr/share/common-lisp/source/${PN}" \
		$(use_enable static) $(use_enable shared)
	cd "${S}"
}
src_compile() {
	cd "${S}"/src
	emake || die "Make failed"
	cd "${S}"
}
src_install() {
	cd "${S}/src"
	common-lisp-install *.lisp dswm.asd
	common-lisp-system-symlink
	emake DESTDIR="${D}" install || die "Install failed"
	cd "${S}"
	dodoc README NEWS || die
	use doc && doinfo doc/dswm.info
}
pkg_postinst() {
	common-lisp_pkg_postinst
	if use shared; then
		elog "When both shared and static modes enabled, 'make install' creates 2 files:"
		elog "/usr/bin/dswm - script, for loading DSWM"
		elog "(you can use it with $LISP env variable: LISP=clisp dswm)"
		elog "and /usr/bin/dswm.static - static binary,"
		elog "else 'make install' creates only ${prefix}/bin/dswm"
	fi
}
